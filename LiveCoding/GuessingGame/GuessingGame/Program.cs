﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GuessingGame
{
    class Program
    {
        static void Main(string[] args)
        {
            int playerGuess;
            string playerInput;
            bool isNumberGuessed = false;
            int numberOfGuesses = 0;
            int theAnswer;

            Random rng = new Random();
            theAnswer = rng.Next(1, 21);

            do
            {
                Console.Write("Enter your guess: ");
                playerInput = Console.ReadLine();

                if ((int.Parse(playerInput) > 20) || (int.Parse(playerInput) < 1))
                {
                    Console.WriteLine("You guessed out of range try again!");
                    continue;
                }
                else
                {
                    playerGuess = int.Parse(playerInput);
                    numberOfGuesses++;
                }

                if (playerGuess == theAnswer)
                {
                    Console.WriteLine("You got it!\nYou guessed {0} times!", numberOfGuesses);
                    isNumberGuessed = true;
                }
                else
                {
                    if (playerGuess > theAnswer)
                    {
                        Console.WriteLine("Too high!");
                    }
                    else
                    {
                        Console.WriteLine("Too low!");
                    }
                }

            } while (!isNumberGuessed);

            Console.ReadLine();
        }
    }
}
