﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ForEach
{
    class Program
    {
        static void Main(string[] args)
        {
            char[] someLetters = { 'a', 'b', 'c', 'd', 'e', 'g', 'h', 'i', 'j' };
            int[] someNumbers = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };

            foreach (var letter in someLetters)
            {
                Console.WriteLine(letter);
            }

            foreach (var number in someNumbers)
            {
                if (number % 2 != 0)
                {
                    Console.WriteLine(number);
                }
            }

            Console.ReadLine();
        }
    }
}
