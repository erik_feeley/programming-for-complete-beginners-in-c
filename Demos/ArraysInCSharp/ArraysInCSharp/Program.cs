﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArraysInCSharp
{
    class Program
    {
        static void Main(string[] args)
        {
            SingleDimensionArrayCreation();
            AutoInitializingArrays();
        }        

        private static void SingleDimensionArrayCreation()
        {
            Console.WriteLine("Create a single dimension array");
            // Create and fill an array of 3 integers
            int[] numbers = new int[3];
            numbers[0] = 100;
            numbers[1] = 200;
            numbers[2] = 300;

            for (int i = 0; i < numbers.Length; i++)
            {
                Console.WriteLine(numbers[i]);
            }

            Console.WriteLine("\nPress enter to continue.");
            Console.ReadLine();
        }

        private static void AutoInitializingArrays()
        {
            Console.WriteLine("=> Array Initialization.");

            // Array initialization syntax using the new keyword.
            string[] stringArray = new string[] { "one", "two", "three" };
            Console.WriteLine("stringArray has {0} elements", stringArray.Length);
        }
    }
}
